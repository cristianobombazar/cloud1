package com.exercicio.cloud1server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Cloud1serverApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cloud1serverApplication.class, args);



	}
}
